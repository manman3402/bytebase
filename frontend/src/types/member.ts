export type MemberStatus = "INVITED" | "ACTIVE";

export type RoleType = "OWNER" | "DBA" | "DEVELOPER";
