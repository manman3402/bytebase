import slug from "slug";
import { DataSourceType, Instance } from "@/types/proto/v1/instance_service";
import { Engine, State } from "@/types/proto/v1/common";
import { Environment } from "@/types/proto/v1/environment_service";
import { keyBy } from "lodash-es";

export const instanceV1Slug = (instance: Instance): string => {
  return [slug(instance.title), instance.uid].join("-");
};

export function instanceV1Name(instance: Instance) {
  let name = instance.title;
  if (instance.state === State.DELETED) {
    name += " (Archived)";
  }
  return name;
}

export const extractInstanceResourceName = (name: string) => {
  const pattern = /(?:^|\/)instances\/([^/]+)(?:$|\/)/;
  const matches = name.match(pattern);
  return matches?.[1] ?? "";
};

export const hostPortOfInstanceV1 = (instance: Instance) => {
  const ds =
    instance.dataSources.find((ds) => ds.type === DataSourceType.ADMIN) ??
    instance.dataSources[0];
  if (!ds) {
    return "";
  }
  const parts = [ds.host];
  if (ds.port) {
    parts.push(ds.port);
  }
  return parts.join(":");
};

// Sort the list to put prod items first.
export const sortInstanceV1ListByEnvironmentV1 = <T extends Instance>(
  list: T[],
  environmentList: Environment[]
): T[] => {
  const environmentMap = keyBy(environmentList, (env) => env.name);

  return list.sort((a, b) => {
    const aEnvOrder = environmentMap[a.environment]?.order ?? -1;
    const bEnvOrder = environmentMap[b.environment]?.order ?? -1;

    return -(aEnvOrder - bEnvOrder);
  });
};

export const supportedEngineV1List = () => {
  const engines: Engine[] = [
    Engine.MYSQL,
    Engine.POSTGRES,
    Engine.TIDB,
    Engine.SNOWFLAKE,
    Engine.CLICKHOUSE,
    Engine.MONGODB,
    Engine.REDIS,
    Engine.SPANNER,
    Engine.ORACLE,
    Engine.OCEANBASE,
    Engine.MARIADB,
    Engine.MSSQL,
    Engine.REDSHIFT,
  ];
  return engines;
};

// export const useInstanceEditorLanguage = (
//   instance: MaybeRef<Instance | undefined>
// ) => {
//   return computed((): Language => {
//     return languageOfEngine(unref(instance)?.engine);
//   });
// };

// export const isValidSpannerHost = (host: string) => {
//   const RE =
//     /^projects\/(?<PROJECT_ID>(?:[a-z]|[-.:]|[0-9])+)\/instances\/(?<INSTANCE_ID>(?:[a-z]|[-]|[0-9])+)$/;
//   return RE.test(host);
// };

// export const instanceHasAlterSchema = (
//   instanceOrEngine: Instance | EngineType
// ): boolean => {
//   const engine = engineOfInstance(instanceOrEngine);
//   if (engine === "REDIS") return false;
//   return true;
// };

// export const instanceHasBackupRestore = (
//   instanceOrEngine: Instance | EngineType
// ): boolean => {
//   const engine = engineOfInstance(instanceOrEngine);
//   if (engine === "MONGODB") return false;
//   if (engine === "REDIS") return false;
//   if (engine === "SPANNER") return false;
//   if (engine === "REDSHIFT") return false;
//   return true;
// };

// export const instanceHasReadonlyMode = (
//   instanceOrEngine: Instance | EngineType
// ): boolean => {
//   const engine = engineOfInstance(instanceOrEngine);
//   if (engine === "MONGODB") return false;
//   if (engine === "REDIS") return false;
//   return true;
// };

export const instanceV1HasCreateDatabase = (
  instanceOrEngine: Instance | Engine
): boolean => {
  const engine = engineOfInstanceV1(instanceOrEngine);
  if (engine === Engine.REDIS) return false;
  if (engine === Engine.ORACLE) return false;
  return true;
};

// export const instanceHasStructuredQueryResult = (
//   instanceOrEngine: Instance | EngineType
// ): boolean => {
//   const engine = engineOfInstance(instanceOrEngine);
//   if (engine === "MONGODB") return false;
//   if (engine === "REDIS") return false;
//   return true;
// };

export const instanceV1HasSSL = (
  instanceOrEngine: Instance | Engine
): boolean => {
  const engine = engineOfInstanceV1(instanceOrEngine);
  return [
    Engine.CLICKHOUSE,
    Engine.MYSQL,
    Engine.TIDB,
    Engine.POSTGRES,
    Engine.REDIS,
    Engine.ORACLE,
    Engine.MARIADB,
    Engine.OCEANBASE,
  ].includes(engine);
};

export const instanceV1HasSSH = (
  instanceOrEngine: Instance | Engine
): boolean => {
  const engine = engineOfInstanceV1(instanceOrEngine);
  return [
    Engine.MYSQL,
    Engine.TIDB,
    Engine.MARIADB,
    Engine.OCEANBASE,
    Engine.POSTGRES,
    Engine.REDIS,
  ].includes(engine);
};

// export const instanceHasCollationAndCharacterSet = (
//   instanceOrEngine: Instance | EngineType
// ) => {
//   const engine = engineOfInstance(instanceOrEngine);

//   const excludedList: EngineType[] = [
//     "MONGODB",
//     "CLICKHOUSE",
//     "SNOWFLAKE",
//     "REDSHIFT",
//   ];
//   return !excludedList.includes(engine);
// };

export const engineOfInstanceV1 = (instanceOrEngine: Instance | Engine) => {
  if (typeof instanceOrEngine === "number") {
    return instanceOrEngine;
  }
  return instanceOrEngine.engine;
};

export const engineNameV1 = (type: Engine): string => {
  switch (type) {
    case Engine.CLICKHOUSE:
      return "ClickHouse";
    case Engine.MYSQL:
      return "MySQL";
    case Engine.POSTGRES:
      return "PostgreSQL";
    case Engine.SNOWFLAKE:
      return "Snowflake";
    case Engine.TIDB:
      return "TiDB";
    case Engine.MONGODB:
      return "MongoDB";
    case Engine.SPANNER:
      return "Spanner";
    case Engine.REDIS:
      return "Redis";
    case Engine.ORACLE:
      return "Oracle";
    case Engine.MSSQL:
      return "MSSQL";
    case Engine.REDSHIFT:
      return "Redshift";
    case Engine.MARIADB:
      return "MariaDB";
    case Engine.OCEANBASE:
      return "OceanBase";
  }
  return "";
};
